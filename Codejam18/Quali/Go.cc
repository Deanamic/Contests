#include <bits/stdc++.h>
using namespace std;

vector<int> x;

int dx[] = {1,1,0,-1,-1,-1,0,1,0};
int dy[] = {0,1,1,1,0,-1,-1,-1,0};
int g[100][100] = {};

int p(int i, int j) {
    for(int k = 0; k< 9; ++k) {
        if(!g[x[i]+dx[k]][x[j]+dy[k]]) return 1;
    }
    return 0;
}

void print(int i, int j) {
    cout << x[i] << ' ' << x[j] << endl;
}

int solve() {
    int a;
    cin >> a;
    memset(g, 0, sizeof(g));
    x.clear();
    x.push_back(2);
    for(int i = 2; (i + 1) * (i + 1) < a; i += 3) {
        x.push_back(i+3);
    }
    int cnt = 0;
    int x1 = -1, y = -1;
    int i = 0, j = 0;
    while(x1 || y) {
        while(!p(i,j)) {
            j++;
            if(j == x.size()) j = 0, i++;
        }
        print(i,j);
        cin >> x1 >> y;
        g[x1][y] = 1;
    }
}

int main() {
    int n;
    cin >> n;
    for(int i = 0; i < n; ++i) {
        solve();
    }
}
