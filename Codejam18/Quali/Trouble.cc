#include <bits/stdc++.h>
using namespace std;

int solve() {
    int n;
    vector<int> a[2];
    cin >> n;
    for(int i = 0; i < n; ++i) {
        int x;
        cin >> x;
        a[i&1].push_back(x);
    }
    sort(a[0].begin(), a[0].end());
    sort(a[1].begin(), a[1].end());
    for(int i = 0; i < n/2; ++i) {
        if(a[0][i] > a[1][i]) return cout << 2*i << endl, 0;
        if(i+1 < a[0].size() && a[1][i] > a[0][i+1]) return cout << 2*i + 1 << endl, 0;
    }
    cout << "OK\n";
}

int main() {
    int n;
    cin >> n;
    for(int i = 0; i < n; ++i) {
        cout << "Case #" << i + 1 << ": ";
        solve();
    }
}
