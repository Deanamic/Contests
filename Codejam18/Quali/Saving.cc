#include <bits/stdc++.h>
using namespace std;


int eval(string s) {
    long long ans = 0;
    long long cnt = 0;
    for(char c : s) {
        if(c == 'S') ans += 1<<cnt;
        else ++cnt;
    }
    return ans;
}
int solve() {
    int D;
    string s;
    cin >> D >> s;
    int mn, cnt;
    mn = cnt = 0;
    int it = 0;
    for(char c : s) {
        if(c == 'S') mn++;
    }
    if(D < mn) return cout << "IMPOSSIBLE" << endl, 0;
    while(eval(s) > D) {
        int ch = s.size() - 2;
        while(s[ch] !=  'C' || s[ch+1] != 'S') --ch;
        swap(s[ch], s[ch+1]);
        it++;
    }
    cout << it << endl;
    return 0;
}
int main() {
    int n;
    cin >> n;
    for(int i = 0; i < n; ++i) {
        cout << "Case #" << i + 1 << ": ";
        solve();
    }
}
