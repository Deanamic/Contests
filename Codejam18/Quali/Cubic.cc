#include <bits/stdc++.h>
using namespace std;

const int IT = 500;
template <class T>
struct Point2 {
    typedef Point2 P;
    T x, y;
    explicit Point2(T x=0, T y=0) : x(x), y(y) {}
    bool operator<(P p) const { return tie(x,y) < tie(p.x,p.y); }
    bool operator==(P p) const { return tie(x,y)==tie(p.x,p.y); }
    P operator+(P p) const { return P(x+p.x, y+p.y); }
    P operator-(P p) const { return P(x-p.x, y-p.y); }
    P operator*(T d) const { return P(x*d, y*d); }
    P operator/(T d) const { return P(x/d, y/d); }
    T dot(P p) const { return x*p.x + y*p.y; }
    T cross(P p) const { return x*p.y - y*p.x; }
    T cross(P a, P b) const { return (a-*this).cross(b-*this); }
    T dist2() const { return x*x + y*y; }
    double dist() const { return sqrt((double)dist2()); }
    // angle to x-axis in interval [-pi, pi]
    double angle() const { return atan2(y, x); }
    P unit() const { return *this/dist(); } // makes dist()=1
    P perp() const { return P(-y, x); } // rotates +90 degrees
    P normal() const { return perp().unit(); }
    // returns point rotated 'a' radians ccw around the origin
    P rotate(double a) const {
        return P(x*cos(a)-y*sin(a),x*sin(a)+y*cos(a)); }

    void print() {
        cout << x << ' ' << y << endl;
    }
};

template <class T>
struct Point{
    T x,y,z;
    Point(T a = 0, T b = 0, T c = 0) : x(a), y(b), z(c){}

    Point operator+(const Point& p) const {
        return Point(x+p.x, y+p.y, z+p.z);
    }
    Point operator-(const Point& p) const {
        return Point(x-p.x, y-p.y, z-p.z);
    }
    Point operator*(const auto p) const {
        return Point(x*p, y*p, z*p);
    }
    Point operator/(const auto p) const {
        return Point(x/p, y/p, z/p);
    }
    Point2<T> project() {
        return Point2<T>(x,z);
    }

    void print() {
        cout << x << ' ' << y << ' ' << z << endl;
    }
};

template <class T>
struct Rotation{
    T P[3][3];
    Rotation(int axis, long double alpha) {
        int nxt = 0;
        long double val[] = {cos(alpha), -sin(alpha), sin(alpha), cos(alpha)};
        for(int i = 0; i < 3; ++i) {
            for(int j = 0; j < 3; ++j) {
                if(i == axis && j == axis) P[i][j] = 1;
                else if(i == axis || j == axis) P[i][j] = 0;
                else P[i][j] = val[nxt++];
            }
        }
    }

    Point<T> operator*(const Point<T>& p) const{
        return Point<T>(P[0][0] * p.x + P[0][1] * p.y + P[0][2] * p.z,
                        P[1][0] * p.x + P[1][1] * p.y + P[1][2] * p.z,
                        P[2][0] * p.x + P[2][1] * p.y + P[2][2] * p.z);
    }
};

template<class T>
vector<Point2<T>> getCorner(Point<T> p1, Point<T> p2, Point<T> p3 ) {
    Point<T> O;
    Point<T> p[][2] = {{p1, O-p1},{p2,O-p2},{p3, O-p3}};
    vector<Point2<T>> ans;
    // for(int i = 0; i < 3; ++i) {
        // for(int j = 0; j < 2; ++j) {
            // p[i][j].print();
        // }
    // }

    for(int i = 0; i < 8; ++i) {
        auto B = (p[0][i&1] + p[1][(i&2)>>1] + p[2][(i&4)>>2]);
        ans.push_back(B.project());
    }
    return ans;
}

typedef Point2<long double> P;
pair<vector<int>, vector<int>> ulHull(const vector<P>& S) {
    vector<int> Q(int(S.size())), U, L;
    iota(Q.begin(), Q.end(), 0);
    sort(Q.begin(), Q.end(), [&S](int a, int b){ return S[a] < S[b]; });
    for(auto it : Q) {
#define ADDP(C, cmp) while ((int)(C.size()) > 1 && S[C[(int)(C.size())-2]].cross( \
    S[it], S[C.back()]) cmp 0) C.pop_back(); C.push_back(it);
        ADDP(U, <=); ADDP(L, >=);
    }
    return {U, L};
}

vector<Point2<long double>> convexHull(const vector<P>& S) {
    vector<int> u, l; tie(u, l) = ulHull(S);
    l.insert(l.end(), u.rbegin()+1, u.rend()-1);
    vector<Point2<long double>> Hull;
    for(auto i : l) Hull.push_back(S[i]);
    return Hull;
}

long double Area(vector<Point2<long double>> v) {
    long double ans = 0;
    for(int i = 0; i < v.size(); ++i) {
        ans +=  v[i].cross(v[(i+1)%((int)v.size())]);
    }
    return abs(ans)/2;
}

int solve() {
    long double A;
    cin >> A;
    Point<long double> P1(0.5,0,0), P2(0, 0.5, 0), P3(0, 0, 0.5);
    //auto debug = getCorner(P1, P2, P3);
    //for(auto p : debug) p.print();
    //debug = convexHull(debug);
    //for(auto p : debug) p.print();
    //cout << Area(debug)<< endl;
    //return 0;
    long double left = 0, right = M_PI/4;
    for(int i = 0; i < IT; ++i) {
        long double mid = (left + right)/2;
        Rotation<long double> R(0, mid);
        auto p1 = R*P1, p2 = R*P2, p3 = R*P3;
        //p1.print(); p2.print(); p3.print();
        auto C = getCorner(p1, p2, p3);
        C = convexHull(C);
        //cout << C.size() << endl;
        long double cur = Area(C);
        //cout << cur << endl;
        if(cur < A) left = mid;
        else right = mid;
    }
    Rotation<long double> R(0, left);
    P1 = R*P1, P2 = R*P2, P3 = R*P3;
    left = 0, right = M_PI/4;
    for(int i = 0; i < IT; ++i) {
        long double mid = (left + right)/2;
        Rotation<long double> R(2, mid);
        auto p1 = R*P1, p2 = R*P2, p3 = R*P3;
        auto C = getCorner(p1, p2, p3);
        long double cur = Area(convexHull(C));
        if(cur < A) left = mid;
        else right = mid;
    }
    Rotation<long double> R1(2, left);
    P1 = R1*P1, P2 = R1*P2, P3 = R1*P3;
    P1.print();
    P2.print();
    P3.print();
}

int main() {
    cout.setf(ios::fixed);
    cout.precision(14);
    int n;
    cin >> n;
    for(int i = 0; i < n; ++i) {
        cout << "Case #" << i + 1 << ":\n";
        solve();
    }
}
