#include <bits/stdc++.h>
using namespace std;

int solve(int n, int l) {
    int v[n] = {};
    int lft = n;
    for(int i = 0; i < l; ++i) {
        cin >> v[i];
        lft -= v[i];
    }

    int mx = (n+1)/2;
    sort(v, v+n, [n](auto a, auto b) {
            if((100*a)%n > n/2) return false;
            if((100*b)%n > n/2) return true;
            return (100*a)%n > (100*b)%n;
        });
    for(int i = 0; lft > 0 && i < n; ++i) {
        while(lft > 0 && (100*v[i])%n < (n+1)/2) {
            v[i]++;
            lft--;
        }
    }
    for(int i = 0; i < n; ++i) v[i] *= 100;
    int ans = 0;
    for(int i = 0; i < n; ++i) {
        ans += (v[i])/n;
        if(v[i]%n >= mx) ans++;
    }
    return ans;
}


int main() {
    int N;
    cin >> N;
    for(int I = 1; I <= N; ++I) {
        int n, l;
        cin >> n >> l;
        printf("Case #%d: %d\n", I, solve(n,l));
    }
}
