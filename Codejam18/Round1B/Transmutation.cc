#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> adj;
int v[128];
int vis[128];

int can(int u) {
    vis[u] = 2;
    if(v[u]) {
        vis[u] = 0;
        --v[u];
        return 1;
    }
    int v1 = adj[u][0], v2 = adj[u][1];
    if((vis[v1] == 2 && v[v1] == 0) || (vis[v2] == 2 && v[v2] == 0)) return 0;
    if(can(v1) && can(v2)) return vis[u] = 0, 1;
    return 0;
}

int solve() {
    int n;
    cin >> n;
    adj = vector<vector<int>> (n);
    for(int i = 0; i < n; ++i) {
        int x,y;
        cin >> x >> y;
        --x, --y;
        adj[i].push_back(x);
        adj[i].push_back(y);
    }
    for(int i = 0; i < n; ++i) cin >> v[i];
    int ans = 0;
    memset(vis, 0, sizeof(vis));


    while(can(0)) {
        ans++;
        memset(vis, 0, sizeof(vis));
        // for(int i = 0; i < n; ++i) cout << v[i] << ' ';
        // cout <<endl;
    }
    return ans;
}


int main() {
    int N;
    cin >> N;
    for(int I = 1; I <= N; ++I) {
        printf("Case #%d: %d\n", I, solve());
    }
}
