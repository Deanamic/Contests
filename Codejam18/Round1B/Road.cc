#include <bits/stdc++.h>
using namespace std;

int a[100100], b[100100];
int n;

int chk(int I, int J, int v1, int v2) {
    for(int i = I; i < J; ++i) {
        if(a[i] != v1 && b[i] != v2) return false;
    }
    return true;
}

int can(int m) {
    for(int i = 0; i + m <= n; ++i) {
        int t = a[i], bt = b[i];
        bool bad = 0;
        for(int j = i; !bad && j < i + m; ++j) {
            if(a[j] == t || b[j] == bt) continue;
            else {
                if(chk(i, i+m, t, b[j])) return true;
                else if(chk(i, i+m, a[j], bt)) return true;
                else bad = 1;
            }
        }
        if(!bad) return true;
    }
    return false;
}

int cnt(int m) {
    int ans = n-m+1;
    for(int i = 0; i + m <= n; ++i) {
        int t = a[i], bt = b[i];
        bool bad = 0;
        for(int j = i; !bad && j < i + m; ++j) {
            if(a[j] == t || b[j] == bt) continue;
            else {
                if(chk(i, i+m, t, b[j]));
                else if(chk(i, i+m, a[j], bt));
                else ans--;
                break;
            }
        }
    }
    return ans;
}
int solve() {

    scanf("%d",&n);
    for(int i = 0; i < n; ++i) {
        int x,y,z;
        scanf("%d%d%d",&x,&y,&z);
        a[i] = x + y;
        b[i] = x - z;
    }
    int l = 0, r = n+1;
    while(l + 1 < r) {
        int m = (l+r)>>1;
        if(can(m)) l = m;
        else r = m;
    }
    int ans = cnt(l);
    printf("%d %d\n", l, ans);
    //cout << l << ' ' << ans << endl;
}


int main() {
    int N;
    scanf("%d",&N);
    for(int I = 1; I <= N; ++I) {
        printf("Case #%d: ", I);
        solve();
    }
}
