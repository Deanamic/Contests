#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
const long double eps = 1e-6;

ll sq(pair<ll, ll> p) {
    return p.first*p.first + p.second*p.second;
}

ll mn(pair<ll, ll> p) {
    return min(p.first, p.second);
}

int solve() {
    long long cur = 0;
    long long curadd = 0;
    long double mx = 0;

    int n;
    long long P;
    cin >> n >> P;
    vector<pair<ll,ll>> v;
    for(int i = 0; i < n; ++i) {
        int x, y;
        cin >> x >> y;
        v.emplace_back(x,y);
        cur += 2*(x+y);
        curadd += 2*mn(v[i]);
        mx += 2.*sqrt(sq(v[i])) + 2.*(x+y);
    }
    if(curadd + cur >= P) {
        sort(v.begin(), v.end(), [](pair<ll, ll> a, pair<ll, ll> b) {
                return mn(a) < mn(b);
            });
        long double lim = 0;
        for(int i = 0; i < n; ++i) {
            if(cur + 2*mn(v[i]) > P) {
                cout << cur + lim << endl;
                return 0;
            }
            else {
                lim += 2.0*sqrt(sq(v[i]));
                if(cur + lim + eps >= P) return cout << (long double)P << endl, 0;
            }
        }
        return cout << (long double)P << endl, 0;
    }
    else {
        if(mx > P+eps ) cout << (long double) P  << endl;
        else cout << mx << endl;
    }
    return 0;
}

int main() {
    cout.setf(ios::fixed);
    cout.precision(6);
    int T;
    cin >> T;
    for(int i = 1; i <= T; ++i) {
        cout << "Case #" << i << ": ";
        solve();
    }
}
