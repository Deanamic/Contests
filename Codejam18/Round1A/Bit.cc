#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1024;
long long m[MAXN], s[MAXN], p[MAXN];
long long r, b, c;

long long best(long long M, int i) {
    long long l1 = 0, r1 = m[i] + 1;
    while(l1 + 1 < r1) {
        long long mid = (l1 + r1)>>1;
        if(mid*s[i] + p[i] <= M) l1 = mid;
        else r1 = mid;
    }
    return l1;
}

bool can(long long M) {
    long long cnt = 0;
    vector<long long> mx;
    for(int i = 0; i < c; ++i) {
        mx.push_back(max(0LL,best(M, i)));
    }
    sort(mx.rbegin(), mx.rend());
    for(int i = 0; i < r; ++i) {
        cnt += mx[i];
    }
    return (cnt >= b);
}

long long solve(){

    cin >> r >> b >> c;
    for(int i = 0; i < c; ++i) {
        cin >> m[i] >> s[i] >> p[i];
    }

    long long L = -1, R = 3*(1e18);
    while(L + 1 < R) {
        long long M = (L + R) >> 1;
        if(can(M)) R = M;
        else L = M;
    }
    return R;
}

int main() {
    int T;
    cin >> T;
    for(int i = 1; i <= T; ++i) printf("Case #%d: %lld\n", i, solve());
}
