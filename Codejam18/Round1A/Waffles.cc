#include <bits/stdc++.h>
using namespace std;

const int MAXN = 128;
char v[MAXN][MAXN];
int pre[MAXN][MAXN];
int R,C,H,V;

int get(int x1, int y1, int x2, int y2) {
    --x1, --y1;
    int q1 = (x2 >= 0 && y2 >= 0 ? pre[x2][y2] : 0);
    int q2 = (x1 >= 0 && y1 >= 0 ? pre[x1][y1] : 0);
    int q3 = (x2 >= 0 && y1 >= 0 ? pre[x2][y1] : 0);
    int q4 = (x1 >= 0 && y2 >= 0 ? pre[x1][y2] : 0);
    return q1 + q2 - q3 - q4;
}

int greedy(int cut, int cnt) {
    int d = 0;
    vector<int> v;
    for(int i = 0; i < C; ++i) {
        if(get(0, 0, cut, i) == (v.size() + 1)*
    }

}

int solve(int I) {

    cin >> R >> C >> H >> V;

    int cnt = 0;
    for(int i = 0; i < R; ++i) {
        for(int j = 0; j < C; ++j){
            cin >> v[i][j];
            if(v[i][j] == '@') ++cnt;
        }
    }
    for(int i = 0; i < R; ++i) {
        for(int j = 0; j < C; ++j){
            int u = (j? pre[i][j-1] : 0);
            int l = (i? pre[i-1][j] : 0);
            int m = (j && i ? pre[i-1][j-1] : 0);
            pre[i][j] = u + l + m + (v[i][j] == '@');
        }
    }
    int p = (h+1)(v+1);
    if(cnt%p) {
        printf("Case #%d: IMPOSSIBLE\n", I);
        return;
    }
    cnt /= p;
    for(int i = 0; i < R; ++i) {
        int val = get(0,0,i,C-1);
        if(val%(V+1) == 0 && val/(V+1) == cnt) {
            if(greedy(i, cnt)) {
                printf("Case #%d: POSSIBLE\n", I);
                return;
            }
        }
    }
    printf("Case #%d: IMPOSSIBLE\n", I);
    return;


}

int main() {
    int T;
    cin >> T;
    for(int i = 1; i <= T; ++i) solve(i);
}
