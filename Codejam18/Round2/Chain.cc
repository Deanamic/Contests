#include <bits/stdc++.h>
using namespace std;

int tri[1000];
int dp[510][510];
void calc() {
    memset(dp, -1, sizeof(dp));
    dp[0][0] = 0;
    for(int I = 1; I < 35; ++I) {
        for(int J = 1; J < 35; ++J) {
            for(int i = 505-I; i >= 0; --i) {
                for(int j = 505-J; j >= 0; --j) {
                    if(dp[i][j] >= 0) dp[i+I][j+J] = max(dp[i+I][j+J], dp[i][j] + 1);
                }
            }
        }
    }
    for(int i = 0; i < 501; ++i) {
        for(int j = 0; j < 501; ++j) {
            if(i) dp[i][j] = max(dp[i][j], dp[i-1][j]);
            if(j) dp[i][j] = max(dp[i][j], dp[i][j-1]);
        }
    }
}

int solve() {
    int r, b;
    cin >> r >> b;
    int ans = 0;
    for(int i = 0; i < 100; ++i) {
        for(int j = 0; j < 100; ++j) {
            int curr = r - tri[i];
            int curb = b - tri[j];
            if(curr < 0 || curb < 0) continue;
            ans = max(ans, i + j + dp[curr][curb]);
        }
    }
    cout << ans << endl;
    return 0;
}

int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    tri[0] = 0;
    tri[1] = 1;
    for(int i = 2; i < 1000; ++i) {
        tri[i] = tri[i-1] + i;
    }
    calc();
    int T;
    cin >> T;
    for(int I = 1; I <= T; ++I) {
        cout << "Case #" << I << ": ";
        solve();
    }
}
