#include <bits/stdc++.h>
using namespace std;

vector<vector<char>> g;
int vis[50][50];
int dx[] = {1,-1,0,0};
int dy[] = {0,0,1,-1};
int r,c;
char nw, ne, sw, se;

int dfs(int x1, int y1, int x, int y) {
    if(x < 1 || y < 1 || x > r || y > c) return 0;
    if(x < x1 && y < y1 && g[x][y] != nw) return 0;
    if(x >= x1 && y < y1 && g[x][y] != sw) return 0;
    if(x < x1 && y >= y1 && g[x][y] != ne) return 0;
    if(x >= x1 && y >= y1 && g[x][y] != se) return 0;
    if(vis[x][y]) return 0;
    vis[x][y] = 1;
    int ans = 1;
    for(int i = 0; i < 4; ++i) {
        int nx = x + dx[i];
        int ny = y + dy[i];
        ans += dfs(x1, y1, nx, ny);
    }
    return ans;
}
void solve() {
    cin >> r >> c;
    g = vector<vector<char>>(50, vector<char>(50,'.'));
    char chk[] = {'B', 'W', '.', '.'};
    for(int i = 1; i <= r; ++i) for(int j = 1; j <= c; ++j) {
            cin >> g[i][j];
        }
    vector<int> ex;
    for(int I = 0; I <= 0xff; ++I) {
        nw = chk[(I&0b11000000)>>6];
        sw = chk[(I&0b110000)>>4];
        ne = chk[(I&0b1100)>>2];
        se = chk[I&3];
        bool add = 0;
        for(int i = 0; i <= r; ++i) {
            for(int j = 0; j <= c; ++j) {
                if(g[i][j] == nw && g[i][j+1] == ne && g[i+1][j] == sw && g[i+1][j+1] == se) add = 1;
            }
        }
        if(add) ex.push_back(I);
    }
    chk[3] = chk[2] = ',';
    int ans = 0;
    for(int i = 0; i <= r; ++i) {
        for(int j = 0; j <= c; ++j) {
            for(int I : ex) {
                nw = chk[(I&0b11000000)>>6];
                sw = chk[(I&0b110000)>>4];
                ne = chk[(I&0b1100)>>2];
                se = chk[I&3];
                memset(vis, 0, sizeof(vis));
                ans = max(dfs(i,j,i,j), ans);
                memset(vis, 0, sizeof(vis));
                ans = max(dfs(i,j,i,j+1), ans);
                memset(vis, 0, sizeof(vis));
                ans = max(dfs(i,j,i+1,j), ans);
                memset(vis, 0, sizeof(vis));
                ans = max(dfs(i,j,i+1,j+1), ans);
            }
        }
    }
    cout << ans << endl;
}

int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    int T;
    cin >> T;
    for(int I = 1; I <= T; ++I) {
        cout << "Case #" << I << ": ";
        solve();
    }
}
