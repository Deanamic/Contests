#include <bits/stdc++.h>
using namespace std;

int solve() {
    int c;
    cin >> c;
    int b[c];
    for(int i = 0; i < c; ++i) {
        cin >> b[i];
    }
    int cnt = 0;
    if(b[0] == 0 || b[c-1] == 0)
        return cout << "IMPOSSIBLE\n", 0;
    vector<vector<char>> ans(250, vector<char>(c,'.'));;
    int l = 0;
    for(int i = 0; i < c; ++i) {
        //cerr << l << endl;
        if(b[i]) { //me hacen falta?
            //cojo los de la izquierda
            int posl = i - l;
            for(int j = 1; j <= posl; ++j) {
                ans[j][i-j] = '\\';
                cnt = max(cnt, j);
            }
            l += b[i];
            b[i] -= max(0,posl);
            //cojo el mio
            //if(b[i] > 0) b[i]--;
            //cojo los de la derecha
            if(b[i] > 0) {
                 for(int j = 1; i+j < l; ++j) {
                    ans[j][i+j] = '/';
                    cnt = max(cnt, j);
                }
            }
        }
    }
    cout << cnt + 1 << '\n';
    for(int i = cnt; i >= 0; --i) {
        for(auto x : ans[i]) cout << x;
        cout << '\n';
    }
    return 0;

}

int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    int T;
    cin >> T;
    for(int I = 1; I <= T; ++I) {
        cout << "Case #" << I << ": ";
        solve();
    }
}
