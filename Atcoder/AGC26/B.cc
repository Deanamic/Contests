#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

const ll oo = 0x3f3f3f3f3f3f3f3fLL;

#define FOR(i, a, b) for(int i = (a); i < int(b); i++)
#define FORD(i, a, b) for(int i = (b)-1; i >= int(a); i--)
#define has(c, e) ((c).find(e) != (c).end())
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()

int solve() {
  ll a,b,c,d;
  cin >> a >> b >> c >> d;
  if(a < b) return cout << "No" << endl, 0;
  if(d < b) return cout << "No" << endl, 0;
  if(c >= b) return cout << "Yes" << endl, 0;
  ll die = b - 1;
  ll st = (a-c)/b;
  st *= b;
  a -= st;
  if(a < b && a > c) return cout << "No" << endl, 0;

}
int main() {
  ios::sync_with_stdio(0); cin.tie(0);
  int T;
  cin >> T;
  while(T--) {
    solve();
  }
	return 0;
}
