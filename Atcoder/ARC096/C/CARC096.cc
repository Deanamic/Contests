#include<bits/stdc++.h>
using namespace std;

int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	long long a, b, c;
	cin >> a >> b >> c;
	b = min(2*c, b);
	a = min(2*c, a);
	long long x,y;
	cin >> x >> y;
	if(2*c >= a+b) {
		return cout << x*a + y*b << endl, 0;
	}
	else {
		int mn = min(x,y);
		return cout << 2*c*mn + (x-mn)*a + (y-mn)*b << endl, 0;
	}
}
