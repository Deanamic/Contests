#include<bits/stdc++.h>
using namespace std;

#include <ext/pb_ds/assoc_container.hpp> // Common file
#include <ext/pb_ds/tree_policy.hpp> // Including tree_order_statistics_node_update
using namespace __gnu_pbds;

template<typename T>
using ordered_set = tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;

#define POS(X,Y) *X.find_by_order(Y)
#define ORD(X,Y) X.order_of_key(Y)

int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	string s;
	cin >> s;
	long long n = s.size();
	long long cnt[26] = {};
	for(char c : s) {
		cnt[c-'a'] ++;
	}
	long long odd = 0;
	long long sp = -1;
	for(long long i = 0; i < 26; ++i) {
		odd += cnt[i]%2;
		if(cnt[i]&1) sp = i;
		cnt[i] >>= 1;
	}
	if(odd > 1) return cout << -1 << endl, 0;

	vector<long long> t(n);
	vector<long long> ord[3];
	for(long long i = 0; i < n; ++i) {
		long long p = s[i] -'a';
		if(p == sp) {
			if(cnt[p] > 0) {
				t[i] = 0;
				--cnt[p];
				ord[0].push_back(i);
			}
			else if(cnt[p] == 0) {
				t[i] = 1;
				--cnt[p];
				ord[1].push_back(i);

			}
			else if(cnt[p] < 0) {
				t[i] = 2;
				ord[2].push_back(i);
			}
		}
		else {
			if(cnt[p]) {
				t[i] = 0;
				--cnt[p];
				ord[0].push_back(i);
			}
			else {
				t[i] = 2;
				ord[2].push_back(i);
			}

		}
	}

	long long pref[3][n];
	for(long long i = 0; i < n; ++i) {
		for(long long j = 0; j < 3; ++j) {
			pref[j][i] = (i ? pref[j][i-1] : 0);
		}
		pref[t[i]][i]++;
	}
	long long inv = 0;
	for(long long i = 0; i < n; ++i) {
		for(long long j = 2; j > t[i]; --j) {
			inv += pref[j][i];
		}
	}
	ordered_set<long long> lol;
	queue<long long> q[26];
	for(long long i = 1; i <= ord[2].size(); ++i) {
		q[s[ord[2][ord[2].size() - i]] - 'a'].push(i);
	}
	for(long long i = 0; i < ord[0].size(); ++i) {
		int p = q[s[ord[0][i]] - 'a'].front();
		q[s[ord[0][i]] - 'a'].pop();
		inv += lol.size() - lol.order_of_key(p);
		lol.insert(p);
	}
	cout << inv << endl;
}
