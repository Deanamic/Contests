#include<bits/stdc++.h>
using namespace std;

vector<int> b;
int n;
bool can(int m) {
	int one = 0, zero = 0;
	if(n-m >= m) {
		return 1;
	}
	for(int i = n-m; i < m; ++i) {
		if(b[i] == 1) one = 1;
		else zero = 1;
	}
	return zero^one;
}

int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	string s;
	cin >> s;
	n = s.size();
	b.resize((int)s.size());
	for(int i = 0; i < int(s.size()); ++i) {
		b[i] = (s[i] == '1');
	}

	int l = 1, r = s.size() + 1;
	while(l + 1 < r) {
		int m = (l + r)/2;
		if(can(m)) l = m;
		else r = m;
	}
	cout << l << endl;
}
