#include<bits/stdc++.h>
using namespace std;

int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	long long X, Y;
	cin >> X >> Y;
	int cnt = 1;
	while(X < Y) {
		X *= 2;
		if(X <= Y) ++cnt;
	}
	cout << cnt << endl;
}
