#include<bits/stdc++.h>
using namespace std;

const long long mod = 1000000007;
const int MAXN = 200010;
vector<int> adj[MAXN];
int sz[MAXN];
int depth[MAXN];
int ans[MAXN];

long long binpow(long long n, long long k) {
	if(k == 0) return 1;
	if(k&1) return (n*binpow((n*n)%mod, k>>1))%mod;
	return binpow((n*n)%mod, k>>1)%mod;
}

long long modinv(long long n, long long k = mod) {
	return binpow(n, k - 2);
}
long long getsz(int u, int d) {
	deoth[d]++;
	sz[u] = 1;
	for(int v : adj[u]) sz[u] += getsz(v);
	return sz[u];
}

long long getans(int u) {
	if(adj[u].size() == 0) return ans[u] = 1;
	vector<long long> one,zero;
	long long allzero = 1;
	for(int v : adj[u]) {
		long long haveone = getans(v);
		long long havezero = ((binpow(2, sz[v]) - haveone)+mod+mod)%mod;
		one.push_back(haveone);
		zero.push_back(havezero);
		allzero *= havezero;
		allzero %= mod;
	}
	long long curans = 0;
	for(int i = 0; i < (int)one.size(); ++i) {
		curans += (((one[i] * modinv(zero[i]))%mod)*allzero)%mod;
	}
	return (curans * binpow(2, sz[u] -1))%mod2;
}

int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n;
	cin >> n;
	for(int i = 1; i <= n; ++i) {
		int p;
		cin >> p;
		adj[p].push_back(i);
	}
	getsz(0, 1);
	for(int i = 1; i < MAXN; ++i) {
		depth[i] += depth[i-1];
	}
	long long res = getans(0);
	cout << res << endl;
}
