//AC
#include<bits/stdc++.h>
using namespace std;

int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n;
	cin >> n;
	pair<int,int> mx = {-1e9,-1};
	pair<int,int> mn = {1e9,-1};
	for(int i = 0; i < n; ++i) {
		int x;
		cin >> x;
		if(x > mx.first) mx = {x,i};
		if(x < mn.first) mn = {x,i};
	}
	cout << 2*n - 1 << endl;
	if(abs(mx.first) > abs(mn.first)) {
		for(int i = 0; i < n; ++i) {
			cout << mx.second + 1 << ' ' << i + 1 << endl;
		}
		for(int i = 0; i + 1< n; ++i) {
			cout << i + 1 << ' ' << i + 2 << endl;
		}
	}
	else {
		for(int i = 0; i < n; ++i) {
			cout << mn.second + 1 << ' ' << i + 1 << endl;
		}
		for(int i = n-1; i >= 1; --i) {
			cout << i + 1 << ' ' << i << endl;
		}
	}
}
