//AC
#include<bits/stdc++.h>
using namespace std;

int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n, k;
	cin >> n >> k;
	map<int,int> a;
	for(int i = 0; i < n; ++i) {
		int x;
		cin >> x;
		++a[x];
	}
	int ans = 0;
	vector<int> v;
	for(auto i : a) v.push_back(i.second);
	sort(v.begin(), v.end());
	for(int i = 0; i < (int)v.size() - k; ++i) {
		ans += v[i];
	}
	cout << ans << endl;
}
